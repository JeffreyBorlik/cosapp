{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "nbsphinx": "hidden"
   },
   "source": [
    "![CoSAppLogo](images/cosapp.svg) **CoSApp** tutorials: Meta-Models"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# What is a `Meta-System`?! ![Experimental feature](images/experimental.svg)\n",
    "\n",
    "A `Meta-System` is an simpler approximation of a complex `System`.\n",
    "\n",
    "When exploring a design space, detailed simulation are usually too expensive to be evaluated at each intermediate tested point. But in order to get a better overall model, an approximated solution would help evaluating the potential of all points. One way to achieve this is to compute the solution with an advanced model or software at few discrete points and to interpolate between those points in exploration phases.\n",
    "\n",
    "## Learn to use it\n",
    "\n",
    "### Create data for a meta-system\n",
    "\n",
    "Start with a `System`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false,
    "nbsphinx": "hidden"
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "import numpy as np\n",
    "\n",
    "logging.getLogger().setLevel(logging.WARNING)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false
   },
   "outputs": [],
   "source": [
    "from cosapp.systems import System\n",
    "from cosapp.ports import Port\n",
    "\n",
    "class XPort(Port):\n",
    "    def setup(self):\n",
    "        self.add_variable('x')\n",
    "\n",
    "class MyExp(System):\n",
    "    def setup(self):\n",
    "        self.add_input(XPort, 'p_in')\n",
    "        self.add_output(XPort, 'p_out')\n",
    "\n",
    "    def compute(self):\n",
    "        self.p_out.x = np.exp(self.p_in.x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a Design of Experiments on this `System`, using an adequate `Driver` (for example, the `LinearDoE` driver)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.drivers import LinearDoE, RunOnce\n",
    "from cosapp.recorders import DataFrameRecorder\n",
    "\n",
    "exp = MyExp('mysin')\n",
    "doe = exp.add_driver(LinearDoE('doe'))\n",
    "doe.add_recorder(DataFrameRecorder(raw_output=True))\n",
    "doe.add_child(RunOnce('run'))\n",
    "\n",
    "doe.add_input_var({'p_in.x': {'lower': 0., 'upper': 3., 'count': 15},})\n",
    "\n",
    "exp.run_drivers()\n",
    "\n",
    "doe.recorder.data.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a meta-system\n",
    "\n",
    "Define its structure (should be consistent with the original `System`!)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.systems import MetaSystem\n",
    "\n",
    "class MetaExp(MetaSystem):\n",
    "    def setup(self):\n",
    "        self.add_input(XPort, 'p_in')\n",
    "        self.add_output(XPort, 'p_out')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instantiate the `MetaSystem` with the data of your DoE. Change the default meta-system if necessary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.systems import ResponseSurface\n",
    "\n",
    "meta_exp = MetaExp('metaexp', doe.recorder.data, default_model=ResponseSurface)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "meta_doe = meta_exp.add_driver(LinearDoE('doe'))\n",
    "meta_doe.add_recorder(DataFrameRecorder(raw_output=True))\n",
    "meta_doe.add_child(RunOnce('run'))\n",
    "\n",
    "meta_doe.add_input_var({'p_in.x': {'lower': 0., 'upper': 3., 'count': 40}})\n",
    "\n",
    "meta_exp.run_drivers()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Use it!\n",
    "\n",
    "For the moment, your `Meta-System` is not trained on your data.\n",
    "So finally, run it to trigger the training."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "meta_exp.run_once()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import plotly.graph_objs as go\n",
    "\n",
    "go.Figure(\n",
    "    data=[\n",
    "        go.Scatter(x=doe.recorder.data['p_in.x'], y=doe.recorder.data['p_out.x'], name=\"exp\"),\n",
    "        go.Scatter(x=meta_doe.recorder.data['p_in.x'], y=meta_doe.recorder.data['p_out.x'], name=\"meta_exp\")\n",
    "    ],\n",
    "    layout=go.Layout(\n",
    "        xaxis={'title': 'p_in.x'},\n",
    "        yaxis={'title': 'p_out.x'}\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Learn more\n",
    "\n",
    "### Computational time\n",
    "\n",
    "A `Meta-System` can achieve time savings by running a simplified version of `System`. The gain will strongly depends on the following characteristics:\n",
    "- the original `System` complexity\n",
    "- the inputs count\n",
    "- the outputs count\n",
    "- the model itself\n",
    "\n",
    "On the previous example, the `Meta-System` is **slower** than the original `System`.\n",
    "\n",
    "Never forget that a `Meta-System` introduces approximations versus the original `System`!\n",
    "\n",
    "### Available models\n",
    "\n",
    "**CoSApp** comes with various models: ResponseSurface, FloatKrigingSurrogate, AnisotropicGP, IsotropicGP, etc.\n",
    "\n",
    "They are available in *cosapp.surrogate_models*. More details are coming soon!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from cosapp.systems import FloatKrigingSurrogate, ResponseSurface, LinearNearestNeighbor, RBFNearestNeighbor, WeightedNearestNeighbor\n",
    "\n",
    "name2class = {\n",
    "    'Kriging': FloatKrigingSurrogate, \n",
    "    'Surface': ResponseSurface, \n",
    "    'LinearNN': LinearNearestNeighbor, \n",
    "    'RBFNN': RBFNearestNeighbor, \n",
    "    'WeightedNN': WeightedNearestNeighbor\n",
    "}\n",
    "    \n",
    "traces = [\n",
    "    go.Scatter(\n",
    "        x=doe.recorder.data['p_in.x'], \n",
    "        y=doe.recorder.data['p_out.x'], \n",
    "        name=\"exp\")\n",
    "]\n",
    "\n",
    "# Run model fitting for all available models\n",
    "for model in name2class:\n",
    "    meta_exp = MetaExp('meta_exp', doe.recorder.data, default_model=name2class[model])\n",
    "\n",
    "    meta_doe = meta_exp.add_driver(LinearDoE('doe'))\n",
    "    meta_doe.add_recorder(DataFrameRecorder(raw_output=True))\n",
    "    meta_doe.add_child(RunOnce('run'))\n",
    "    meta_doe.add_input_var({'p_in.x': {'lower': 0., 'upper': 3., 'count': 40}})\n",
    "\n",
    "    meta_exp.run_drivers()\n",
    "    \n",
    "    traces.append(\n",
    "        go.Scatter(\n",
    "            x=meta_doe.recorder.data['p_in.x'], \n",
    "            y=meta_doe.recorder.data['p_out.x'], \n",
    "            name=model\n",
    "        )\n",
    "    )\n",
    "    \n",
    "\n",
    "go.Figure(\n",
    "    data=traces,\n",
    "    layout=go.Layout(\n",
    "        xaxis={'title': 'p_in.x'},\n",
    "        yaxis={'title': 'p_out.x'},\n",
    "        hovermode='x'\n",
    "    )\n",
    ")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "toc": {
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": "block",
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
