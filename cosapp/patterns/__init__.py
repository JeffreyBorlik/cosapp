from .singleton import Singleton
from .observer import Subject, Observer
